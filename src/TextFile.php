<?php

namespace txd\textfile;

/**
 * This class manages the lines of a text file.
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class TextFile extends \SplFileObject
{
	/**
	 * @var string The file path.
	 */
	public $file;

	/**
	 * @var bool Flag that indicates if EOL should be present at the end of the file.
	 */
	public $ensureEmptyLine = true;

	/**
	 * @var array The lines of the file.
	 */
	private $_lines = [];


	/**
	 * TextFile constructor.
	 * Handles the creation of the file if does not exist.
	 *
	 * @param string $file The full path of the file.
	 * @param bool $create Flag that indicates if the file should be created if does not exist.
	 */
	public function __construct($file, $create = true)
	{
		if ($create === true && !is_file($file)) {
			$dir = pathinfo($file, PATHINFO_DIRNAME);

			if (!is_dir($dir)) {
				mkdir($dir, 0755, true);
			}

			fclose(fopen($file, 'w'));
		}

		parent::__construct($file);

		$this->file = $file;
		$this->_lines = explode("\n", file_get_contents($file));
	}

	/**
	 * Loads or creates a new file.
	 *
	 * @param string $file The full path of the file.
	 * @param bool $create Flag that indicates if the file should be created if does not exist.
	 * @return static
	 */
	public static function load($file, $create = true)
	{
		return new static($file, $create);
	}

	/**
	 * Returns the lines of the file.
	 *
	 * @param null|string $filter A filter for the lines to be returned.
	 * @return array The lines of the file.
	 */
	public function getLines($filter = null)
	{
		if (isset($filter)) {
			return array_filter($this->_lines, function ($line) use ($filter) {
				return strpos($line, $filter) !== false;
			});
		}

		return $this->_lines;
	}

	/**
	 * Gets the first line of the file.
	 *
	 * @return string|null the first line of the file.
	 */
	public function getFirstLine()
	{
		return reset($this->_lines);
	}

	/**
	 * Gets the last line of the file.
	 *
	 * @return string|null the last line of the file.
	 */
	public function getLastLine()
	{
		return end($this->_lines);
	}

	/**
	 * Gets the line index by its content.
	 *
	 * @param string $line The content of the line.
	 * @return int A positive integer representing the index of the line or -1 if does not exist.
	 */
	public function getLineIndex($line)
	{
		$lineIndex = -1;

		foreach ($this->_lines as $index => $existingLine) {
			if (strpos($existingLine, $line) !== false) {
				$lineIndex = $index;
				break;
			}
		}

		return $lineIndex;
	}

	/**
	 * Gets the line content by its index.
	 *
	 * @param int $index The line index.
	 * @return string|null The content of the line or null if the line does not exist.
	 */
	public function getLineContent($index)
	{
		return $this->_lines[$index];
	}

	/**
	 * Checks if the file has a specific line.
	 *
	 * @param string|int $line The content of the line or its index.
	 * @return bool The existence of the line.
	 */
	public function hasLine($line)
	{
		if (is_int($line)) {
			return $this->getLineContent($line) !== null;
		}
		return $this->getLineIndex($line) !== -1;
	}

	/**
	 * Deletes a line by content or by its index.
	 *
	 * @param string|int $line tThe content of the line or its index.
	 * @return bool The result of the delete operation.
	 */
	public function deleteLine($line)
	{
		$lineIndex = is_int($line) ? $line : $this->getLineIndex($line);

		if (isset($this->_lines[$lineIndex])) {
			unset($this->_lines[$lineIndex]);
			$this->saveFile();
		}

		return !$this->hasLine($line);
	}

	/**
	 * Deletes multiple lines.
	 *
	 * @param array A list of lines to be deleted.
	 * @return int The number of deleted lines.
	 */
	public function deleteLines($lines)
	{
		$successCount = 0;

		foreach ((array) $lines as $line) {
			$lineIndex = is_int($line) ? $line : $this->getLineIndex($line);

			if (isset($this->_lines[$lineIndex])) {
				unset($this->_lines[$lineIndex]);
				$successCount++;
			}
		}

		if ($successCount > 0) {
			$this->saveFile();
		}

		return $successCount;
	}

	/**
	 * Adds a new line to the file.
	 *
	 * @param string $line The content of the new line.
	 * @return TextLine The instance of the line builder class.
	 */
	public function addLine($line)
	{
		return new TextLine($this, $line);
	}

	/**
	 * Merges a new line to the existing lines.
	 *
	 * @param TextLine $textLine The instance of the TextLine class.
	 * @return int A positive integer representing the index of the new (or existing) line
	 * This method also returns -1 if the new line to be saved does not have content set.
	 */
	protected function mergeLine($textLine)
	{
		if (!isset($textLine->content)) {
			return -1;
		}
		if ($this->hasLine($textLine->content)) {
			return $this->getLineIndex($textLine->content);
		}

		if (isset($textLine->before)) {
			$textLine->before = is_int($textLine->before) ? $textLine->before : $this->getLineIndex($textLine->before);
			array_splice($this->_lines, $textLine->before, 0, $textLine->content);
		} elseif (isset($textLine->after)) {
			$textLine->after = is_int($textLine->after) ? $textLine->after : $this->getLineIndex($textLine->after);
			array_splice($this->_lines, $textLine->after + 1, 0, $textLine->content);
		} else {
			$this->_lines[] = $textLine->content;
		}

		return $this->getLineIndex($textLine->content);
	}

	/**
	 * Saves the content to the file.
	 *
	 * @return int|false The number of bytes that were written to the file, or false on failure.
	 */
	protected function saveFile()
	{
		if ($this->ensureEmptyLine && !empty(trim($this->getLastLine()))) {
			$this->_lines[] = '';
		}

		$this->_lines = array_values($this->_lines);

		return file_put_contents($this->file, implode("\n", $this->_lines));
	}

	/**
	 * Saves the file.
	 *
	 * @param null|string|TextLine $line The line to be saved.
	 * @return bool The file save operation status.
	 */
	public function save($line = null)
	{
		if ($line instanceof TextLine) {
			$this->mergeLine($line);
		} elseif (is_string($line)) {
			$this->mergeLine(new TextLine($this, $line));
		}

		return $this->saveFile() !== false;
	}
}
