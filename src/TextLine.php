<?php

namespace txd\textfile;

/**
 * This class serve as text line builder.
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class TextLine
{
	/**
	 * @var TextFile The TextFile class instance that handles the file.
	 */
	public $textFile;

	/**
	 * @var string|int Before position of this line.
	 */
	public $before;

	/**
	 * @var string|int After position of this line.
	 */
	public $after;

	/**
	 * @var string The content of this line.
	 */
	public $content;


	/**
	 * TextLine constructor.
	 *
	 * @param TextFile $textFile The TextFile class instance that handles the file.
	 * @param string $line The content of this line.
	 */
	public function __construct($textFile, $line)
	{
		$this->textFile = $textFile;
		$this->content = $line;
	}

	/**
	 * Sets the before position of this line.
	 *
	 * @param string|int $line The content of the line or its index.
	 * @return static
	 */
	public function beforeLine($line)
	{
		$this->before = $line;

		return $this;
	}

	/**
	 * Sets the after position of this line.
	 *
	 * @param string|int $line The content of the line or its index.
	 * @return static
	 */
	public function afterLine($line)
	{
		$this->after = $line;

		return $this;
	}

	/**
	 * Saves the line to the file.
	 *
	 * @return int|false The number of bytes that were written to the file, or false on failure.
	 */
	public function save()
	{
		return $this->textFile->save($this);
	}

	/**
	 * PHP magic method that returns the string representation of this object.
	 * @return string The string representation of this object.
	 */
	public function __toString()
	{
		return $this->content;
	}
}
